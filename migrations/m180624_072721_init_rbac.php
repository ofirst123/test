<?php

use yii\db\Migration;

/**
 * Class m180624_072721_init_rbac
 */
class m180624_072721_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;



        // add "updateUser" permission
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $auth->add($updateUser);

        // add "deleteUser" permission
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $auth->add($deleteUser);

         // add "viewUserList" permission
        $viewUserList = $auth->createPermission('viewUserList');
        $viewUserList->description = 'view Post List';
        $auth->add($viewUserList);

        // add "updateOwnUser" permission
        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $updateOwnUser->description = 'Update Own task';

        $rule = new \app\rbac\EmployeeRule;
        $auth->add($rule);

        $updateOwnUser->ruleName = $rule->name;                 
        $auth->add($updateOwnUser); 
        
        // add "createTask" permission
        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'Create a task';
        $auth->add($createTask);

        // add "updateTask" permission
        $updateTask = $auth->createPermission('updateTask');
        $updateTask->description = 'Update task';
        $auth->add($updateTask);

         // add "viewTaskList" permission
        $viewTaskList = $auth->createPermission('viewTaskList');
        $viewTaskList->description = 'view Post List';
        $auth->add($viewTaskList);


         // add "deleteTask" permission
        $deleteTask = $auth->createPermission('deleteTask');
        $deleteTask->description = 'Delete post';
        $auth->add($deleteTask);


        // add "admin" role and give this role  חייב להיות משתמש שיכול לבצע הכל...
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $deleteUser);
        

        // add "manager" role and give this permission
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $updateUser);
        $auth->addChild($manager, $deleteTask);
        $auth->addChild($manager, $updateTask);


         // add "employee" role and give this permission
        $employee = $auth->createRole('employee');
        $auth->add($employee);
        $auth->addChild($employee, $createTask);
        $auth->addChild($employee, $viewTaskList);
        $auth->addChild($employee, $updateOwnUser);
        $auth->addChild($employee, $viewUserList);



        $auth->addChild($admin, $manager);
        $auth->addChild($manager, $employee);
        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($employee, 3);
        $auth->assign($manager, 2);
        $auth->assign($admin, 1);
    }
    

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_072721_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_072721_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
