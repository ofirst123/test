<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_053336_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
        ]);

        $this->insert('urgency', [
            'id' => '1',
            'name' => 'normal',]);
         $this->insert('urgency', [
            'id' => '2',
            'name' => 'low',]);
         $this->insert('urgency', [
            'id' => '3',
            'name' => 'critical',
                
    
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
